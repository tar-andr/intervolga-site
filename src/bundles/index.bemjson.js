module.exports = {
    block: 'page',
    title: 'Пустая',
    content: [
        require('./common/header.bemjson'),
        {block: 'main', content: [
            require('./common/swiper-row-advantage.bemjson'),
            require('./common/info.bemjson'),
            require('./common/clients.bemjson'),
            require('./common/about-project.bemjson'),
            require('./common/main-content.bemjson'),
            require('./common/quality.bemjson'),
            require('./common/start-work.bemjson'),
            require('./common/director.bemjson'),
            require('./common/service.bemjson'),
            require('./common/recommendation.bemjson'),
            require('./common/leader.bemjson'),
            require('./common/footer.bemjson'),
        ]},
    ],
};
