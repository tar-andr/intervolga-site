const CLIENTS = [
    '../../images/bitrix.png',
    '../../images/1c-bitrix.png',
    '../../images/evraz.png',
    '../../images/alfa-bank.png',
    '../../images/mybox.png',
    '../../images/castle-rock.png'];

module.exports = [

    {cls: 'text-gray-700 bg-gray-200 py-3 py-lg-xxl py-xl-xxxl', content: [
        {mix: {block: 'container', mods: {indent: true}}, content: [
            {block: 'h', size: 1, cls: 'font-weight-extrabold', content: 'Кто рекомендует ИНТЕРВОЛГУ?'},
            {block: 'h', size: 3, content: 'Нас может рекомендовать любой из крупных клиентов: '},
            {block: 'swiper-row', cls: 'row my-4', content: [
                {cls: 'w-100', content: [
                    {cls: 'swiper-container swiper-container-horizontal', content: [
                        {cls: 'swiper-wrapper', content: [
                            CLIENTS.map((src)=>[
                                {block: 'col-6', cls: 'col-sm-6 col-md-4 col-lg-6 col-xl-2 swiper-slide h-auto', content: [
                                    {block: 'image', mods: {size: '160x60', align: 'middle'}, content: [
                                        {block: 'img', mods: {lazy: true}, src: src},
                                    ]},
                                ]},
                            ]),
                        ]},
                    ]},
                ]},
            ]},
            {tag: 'p', cls: 'text-gray-500 mt-3', content: 'По запросу дадим прямые контакты руководителей веб-проектов и интранет-порталов, которые мы поддерживаем и продвигаем.'},
        ]},
    ]},
];

