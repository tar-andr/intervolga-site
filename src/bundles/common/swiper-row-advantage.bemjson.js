module.exports = [
    {block: 'advantage', mix: {block: 'swiper-row'}, content: [
        {cls: 'w-100', content: [
            {cls: 'swiper-container swiper-container-horizontal', content: [
                {cls: 'swiper-wrapper', content: [...new Array(6)].map((item, index)=>[
                    {elem: 'slide', cls: 'bg-gray-600 py-xxl pb-xxxl swiper-slide h-auto', content: [
                        {mix: {block: 'container', mods: {indent: true}}, content: [
                            {elem: 'img', attrs: {
                                'data-src': index ? `http://placehold.it/1500x500/0${index}00${index}0` :'../../images/macbook.png',
                                'style': index ? 'opacity: 0;' : 'background-image: url(../../images/macbook.png)',
                            }},
                            {elem: 'body', content: [
                                {elem: 'title', content: 'Cложные задачи для крупных компаний '},
                                {elem: 'description', content: [
                                    {elem: 'text', content: 'Строим системы интернет-продаж. Решаем задачи развивающегося бизнеса. '},
                                    {elem: 'text', content: '1С, интернет-магазины, лидогенерация, сквозная аналитика, триггерные рассылки и динамический ремаркетинг.'},
                                ]},
                                {elem: 'links', content: [
                                    'Интеграция с внутренними учетными системами',
                                    'Интеграция Битрикс24 с сайтом',
                                    'Аудит производительности нагруженных систем',
                                ].map((link)=>[
                                    {elem: 'link', content: [
                                        {block: 'a', content: [
                                            {tag: 'span', content: link},
                                        ]},
                                    ]},
                                ])},
                            ]},
                        ]},
                    ]},
                ])},
                {elem: 'pagination', elemMods: {color: 'white', align: 'left'}, cls: 'swiper-pagination'},
            ]},
        ]},
    ]},
];
