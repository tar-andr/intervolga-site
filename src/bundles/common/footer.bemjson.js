module.exports = [
  {block: 'footer', cls: 'bg-black text-white py-3 py-lg-xxl py-xl-xxxl', content: [
      {mix: {block: 'container', mods: {indent: true}}, content: [
          {cls: 'row', content: [
              {cls: 'col-8', content: [
                  {block: 'getId', content: [
                      {block: 'control'},
                      {block: 'tab', attrs: {id: 'moskow'}},
                  ]},
              ]},
          ]},
      ]},
  ]},
  require('./modals.bemjson'),
];
