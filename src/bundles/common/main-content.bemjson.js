const LINKS = ['1С', 'CRM', 'Агент700', 'Битрикс24', 'Брендинг', 'Маркетинг', 'Проекты', 'Проекты', 'Жизнь'];
const ARTICLES = [
    {
        title: 'Настраиваем аналитику на лендингах и Битрикс24.Сайтах — пошаговое руководство',
        section: 'Битрикс24',
        author: 'Александр|Давыдов',
        img: '../../images/author.jpg',
    },
    {
        title: 'Телефония Asterisk и Битрикс24',
        section: 'Проекты',
        author: 'Анатолий|Ерофеев',
        img: '../../images/author.jpg',
    },
    {
        title: 'Как настроить и использовать "цифровую воронку" amoCRM',
        section: 'CRM',
        author: 'Елена|Федянова',
        img: '../../images/author.jpg',
    },
    {
        title: 'Как настроить и использовать "цифровую воронку" amoCRM',
        section: 'Проекты',
        author: 'Анатолий|Ерофеев',
        img: '../../images/author.jpg',
    },
    {
        title: 'Телефония Asterisk и Битрикс24',
        section: 'Проекты',
        author: 'Анатолий|Ерофеев',
        img: '../../images/author.jpg',
    },
    {
        title: 'Как настроить и использовать "цифровую воронку" amoCRM',
        section: 'Проекты',
        author: 'Елена|Федянова',
        img: '../../images/author.jpg',
    },
];

module.exports = [
    {block: 'main-content', cls: 'bg-gray-200 py-3 py-lg-xxl', content: [
        {mix: {block: 'container', mods: {indent: true}}, content: [
            {block: 'hidden-title', content: [
                {elem: 'backside font-weight-bold text-gray-600', content: 'Блог'},
                {block: 'nav-links', content: LINKS.map((link)=>[
                    {elem: 'item', content: [
                        {elem: 'link', attrs: {href: '#'}, content: link},
                    ]},
                ])},
            ]},
            {cls: 'row', content: ARTICLES.map((article) => [
                {cls: 'col-xl-6 col-xxl-4', content: [
                    {block: 'preview', content: [
                        {elem: 'header', content: [
                            {elem: 'section', content: article.section},
                            {block: 'rating', cls: 'ml-3 text-gray-700'},
                        ]},
                        {elem: 'title', content: article.title},
                        {elem: 'bottom', content: [
                            {elem: 'img', attrs: {
                                style: 'background-image: url(' + article.img + ')',
                            }},
                            {elem: 'author', content: [
                                {tag: 'span', content: article.author.split('|')[0]},
                                {tag: 'span', content: article.author.split('|')[1]},
                            ]},
                            {elem: 'link', mix: {block: 'fi', mods: {icon: 'redirect'}}},
                        ]},
                    ]},
                ]},
            ])},
        ]},
    ]},
];

