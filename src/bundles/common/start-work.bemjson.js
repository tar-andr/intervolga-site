module.exports = [
    {block: 'start-work', cls: 'bg-blue text-white py-3 py-lg-xxl pb-lg-xxxl', content: [
        {mix: {block: 'container', mods: {indent: true}}, content: [
            {elem: 'title', content: 'Как начать работу?'},
            {cls: 'row', content: [
                {cls: 'col-xl-6 col-xxl-4 mt-2', content: [
                    {elem: 'item', content: [
                        {block: 'hidden-title', cls: 'mb-2', content: [
                            {elem: 'backside', content: '01'},
                            {elem: 'frontside', content: 'Все просто'},
                        ]},
                        {block: 'link-opacity', cls: 'badge', content: 'написать нам письмо'},
                    ]},
                ]},
                {cls: 'col-xl-6 col-xxl-4 mt-2', content: [
                    {elem: 'item', content: [
                        {block: 'hidden-title', cls: 'mb-2', content: [
                            {elem: 'backside', content: '02'},
                            {elem: 'frontside', content: 'Есть вопросы? '},
                        ]},
                        {block: 'link-opacity', cls: 'badge mr-l', content: 'блог'},
                        {block: 'link-opacity', cls: 'badge', content: 'портфолио'},
                    ]},
                ]},
                {cls: 'col-xl-6 col-xxl-4 mt-2', content: [
                    {elem: 'item', content: [
                        {block: 'hidden-title', cls: 'mb-2', content: [
                            {elem: 'backside', content: '03'},
                            {elem: 'frontside', content: 'Захватить мир?'},
                        ]},
                        {block: 'link-opacity', cls: 'badge mr-l', content: 'звони прямо сейчас'},
                    ]},
                ]},
            ]},
        ]},
    ]},
];
