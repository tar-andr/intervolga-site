module.exports = [
    {block: 'leader', cls: 'py-3 py-lg-xxl bg-yellow text-white', content: [
        {mix: {block: 'container', mods: {indent: true}, cls: 'position-relative'}, content: [
            {cls: 'row flex-row-reverse', content: [
                {cls: 'col-12 col-sm-4 d-none d-sm-block', content: [
                    {elem: 'img', attrs: {src: '../../images/leader.png'}},
                ]},
                {cls: 'col-12 col-sm-8', content: [
                    {cls: 'row', content: [
                        {cls: 'col-12 col-xxl-7', content: [
                            {block: 'h', size: 1, cls: 'font-weight-extrabold', content: 'Интернет-агентство ИНТЕРВОЛГА'},
                        ]},
                        {cls: 'col-12 col-xxl-5', content: [
                            {block: 'h', size: 1, cls: 'font-weight-light mb-md-3', content: '1 место по ЮФО'},
                            {tag: 'p', content: 'в рейтинге партнеров наиболее распространенной российской платформы для создания коммерческих сайтов 1С-Битрикс'},
                        ]},
                    ]},
                ]},
            ]},
        ]},
    ]},
];
