module.exports = [
    {block: 'about-project', content: [
        {elem: 'frontside', cls: 'bg-blue', content: [
            {mix: {block: 'container', mods: {indent: true}}, content: [
                {elem: 'title', content: 'Расскажи о своем проекте'},
                {elem: 'arrow'},
            ]},
        ]},
        {elem: 'backside', cls: 'bg-primary', content: [
            {mix: {block: 'container', mods: {indent: true}}, content: [
                {elem: 'title', content: 'Расскажи о своем проекте'},
                {elem: 'arrow'},
            ]},
        ]},
    ]},
];

