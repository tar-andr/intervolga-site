module.exports = [
    {cls: 'text-white bg-info py-3 py-lg-xxl pt-xl-xxxl', content: [
        {mix: {block: 'container', mods: {indent: true}}, content: [
            {block: 'columns', cls: 'columns-xl-3 columns-md-1 mb-xl-xxl pb-md', content: [
                {block: 'h', size: 1, cls: 'font-weight-extrabold', content: 'ИНТЕРВОЛГА – компетентный веб-интегратор. '},
                {tag: 'p', content: 'Мы – крупная профессиональная команда дизайнеров, веб-разработчиков и интернет-маркетологов.  <br/><br/> Лучше всего мы работаем в комплексных проектах, где нужен набор наших сильных компетенций: от брендинга, продвижения сайтов и создания сайтов до стратегического интернет-маркетинга и автоматизации бизнес-процессов.'},
                {tag: 'p', content: 'ИНТЕРВОЛГА отличается хорошей репутацией, прозрачностью работы и большими производственными возможностями.'},
            ]},
            {mix: {block: 'row'}, content: [
                {cls: 'col-xxl-4 col-xl-6', content: [
                    {block: 'achievement', content: [
                        {elem: 'icon', content:
                            {block: 'fi', mods: {icon: 'cup'}, cls: 'align-middle'},
                        },
                        {elem: 'description', content: 'Мы входим в список компаний, рекомендованных 1С-Битрикс для разработки крупных проектов.'},
                    ]},
                ]},
                {cls: 'col-xxl-4 col-xl-6', content: [
                    {block: 'achievement', content: [
                        {elem: 'icon', content:
                            {block: 'fi', mods: {icon: 'puzzle'}, cls: 'align-middle'},
                        },
                        {elem: 'description', content: 'В штате больше 50 сотрудников: и ... ни одного "фрилансера на удаленке".'},
                    ]},
                ]},
                {cls: 'col-xxl-4 col-xl-6', content: [
                    {block: 'achievement', content: [
                        {elem: 'icon', content:
                            {block: 'fi', mods: {icon: 'progress'}, cls: 'align-middle'},
                        },
                        {elem: 'description', content: 'Прогресс по каждому проекту работы прозрачен для клиента.'},
                    ]},
                ]},
            ]},
        ]},
    ]},
];
