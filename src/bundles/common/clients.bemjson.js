const ITEMS = [
    {title: 'ecommerce', name: 'Клуб клиентов Альфа-Банка', color: '#D72E34', link: '#'},
    {title: 'личный кабинет', name: 'ЕВРАЗ', color: 'orange', link: '#'},
    {title: 'интернет-магазин', name: 'MYBOX', color: 'green', link: '#'},
    {title: 'информационный портал', name: 'Международного форума Valdai', color: 'indigo', link: '#'},
    {title: 'интернет-магазин', name: 'ЕвроОбувь', color: 'red', link: '#'},
    {title: 'интернет-магазин', name: 'CastleRock', color: '#000000', link: '#'},
];
module.exports = [
    {block: 'clients', cls: 'bg-gray-900', content: [
        {mix: {block: 'container', mods: {indent: true}}, content: [
            {elem: 'row', content: ITEMS.map((item, index) => [
                {elem: 'tile', attrs: {href: item.link}, content: [
                    {elem: 'bg', cls: `bg-${item.color}`, attrs: {style: `background-color: ${item.color}`}},
                    item.title && {elem: 'title', content: item.title},
                    item.name && {elem: 'name', content: item.name},
                ]},
            ])},
        ]},
    ]},
];

