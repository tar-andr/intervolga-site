const DIRECTOR = {
    img: '../../images/director.png',
    name: 'Степан',
    secondName: 'Овчинников',
    post: 'генеральный директор',
};

module.exports = [
    {block: 'director', cls: 'text-white bg-gray-700 py-3 py-lg-xxl', content: [
        {mix: {block: 'container', mods: {indent: true}}, content: [
            {cls: 'row', content: [
                {cls: 'col-xl-12 col-xxl-8', content: [
                    {block: 'h', size: 1, cls: 'mb-3', content: 'Что-то пошло не так?'},
                    {block: 'link-opacity', attrs: {href: '#'}, cls: 'badge align-middle mr-xs-0 mr-xl-3', content: 'Позвоните генеральному директору'},
                    {block: 'h', size: 3, cls: 'align-middle d-inline-block my-0', content: 'на мобильный телефон'},
                ]},
                {cls: 'col-xl-12 col-xxl-4 mt-3 mt-xxl-0 text-center text-xxl-left', content: [
                    {elem: 'img', attrs: {
                        style: 'background-image: url('+DIRECTOR.img+')'}},
                    {elem: 'about', content: [
                        {elem: 'name', cls: 'font-weight-bold', content: DIRECTOR.name},
                        {elem: 'secondname', content: DIRECTOR.secondName},
                        {elem: 'post', content: DIRECTOR.post},
                    ]},
                ]},
            ]},
        ]},
    ]},
];

