module.exports = [
    {
        block: 'header', content: [
        {
            elem: 'title',
            content: [
                {
                    block: 'logo-place',
                    content: [
                        {block: 'logo', mods: {size: 'sm'}, // sm|lg
                            content: [
                            {block: 'img', mods: {lazy: true}, src: '../../images/logo.svg'},
                        ]},
                        {
                            block: 'language',
                            cls: 'dropdown',
                            content: [
                                {
                                    elem: 'btn',
                                    cls: 'dropdown-toggle text-uppercase',
                                    content: 'Russian',
                                },
                                {
                                    elem: 'list',
                                    cls: 'dropdown-menu',
                                    content: ['russian', 'english', 'deutsch', 'spanish'].map((lang)=>[
                                        {
                                            elem: 'item',
                                            content: lang,
                                        },
                                    ]),
                                },
                            ],
                        },
                    ],
                },
                {
                    block: 'nav-btn',
                    mods: {size: 'sm'}, // sm|lg
                    content: new Array(3).fill('').map(()=>[
                        {
                            tag: 'span',
                        },
                    ]),
                },
            ],
        },
        {cls: 'py-2', content: [
            {
                block: 'search', content: [
                {
                    elem: 'btn', content: {block: 'fi', mods: {icon: 'search'}},
                },
                {
                    elem: 'input',
                },
            ],
            },
        ]},
        {elem: 'inner', content: [
            {cls: 'pt-2 pb-4', content: [
                {
                    block: 'navigation',
                    content: [
                        'Создание сайта|на Битрикс',
                        'Интернет-маркетинг|и Лидогенерация',
                        'Поддержка сайтов|можем все и даже больше',
                        'CRM Битрикс24|профессионально внедряем',
                        'Брендинг|Разработка и создание бренда',
                        '1С|сложные задачи для крупных компаний',
                    ].map((index)=>[
                        {elem: 'link', content: [
                            {elem: 'title', cls: 'font-weight-semibold', content: index.split('|')[0]},
                            !!index.split('|')[1] &&
                            {elem: 'subtitle', content: index.split('|')[1]},
                        ]},
                    ]),
                },
            ]},
            {cls: 'pt-2 pb-4', content: [
                {
                    block: 'navigation',
                    content: [
                        'Агенство|основано в 2003 году',
                        'Проекты|больше 500 довольных клиентов',
                        'Блог|Самый популярный в IT',
                        'Вакансии|Мы всегда ищем таланты',
                        'Контакты|Москва, Волгоград и другие города'].map((index)=>[
                        {elem: 'link', content: [
                            {elem: 'title', content: index.split('|')[0]},
                            !!index.split('|')[1] &&
                            {elem: 'subtitle', content: index.split('|')[1]},
                        ],
                        },
                    ]),
                },
            ]},
        ]},
        {elem: 'footer', cls: 'mt-2', content: [
            {block: 'contacts', content: [
                {elem: 'icon', content: [
                    {block: 'fi', cls: 'align-middle', mods: {icon: 'call'}},
                ]},
                {elem: 'text', content: [
                    {block: 'a', href: 'tel:+74956485790', content: '8 (495)  648-57-90'}
                ]},
            ]},
            {block: 'contacts', content: [
                {elem: 'icon', content: [
                    {block: 'fi', cls: 'align-middle', mods: {icon: 'email'}},
                ]},
                {elem: 'text', content: [
                    {block: 'a', href: 'mailto:info@intervolga.ru ', content: 'Напишите нам'}
                ]},
            ]},
        ]},
    ],
    },
];
