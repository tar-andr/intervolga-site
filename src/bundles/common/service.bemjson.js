module.exports = [
    {cls: 'py-3 py-lg-xxl py-xl-xxxl text-gray-700', content: [
        {mix: {block: 'container', mods: {indent: true}}, content: [
            {block: 'columns', cls: 'columns-md-1 columns-xl-3 column-gap-160', content: [
                {block: 'h', size: 1, cls: 'font-weight-extrabold', content: 'Какова стоимость наших услуг?'},
                {block: 'h', size: 2, cls: 'font-weight-light d-inline-block  mt-0', content: 'Принцип: работаем хорошо и берем за это честные деньги.'},
                {tag: 'p', content: 'Для этого улучшаем процессы внутри, изучаем новые технологии, чтобы делать работу правильно и быстро. Один час работы специалиста нашей компании стоит 2400 рублей. <br/><br/> Общение – бесценно, поэтому бесплатно.'},
            ]},
        ]},
    ]},
];

