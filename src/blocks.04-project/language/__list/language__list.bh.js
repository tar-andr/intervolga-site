module.exports = (bh) => {
    bh.match('language__list', (ctx, json) => {
        ctx.attrs({
            'aria-expanded': 'language',
        });
    });
};
