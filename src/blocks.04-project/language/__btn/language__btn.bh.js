module.exports = (bh) => {
    bh.match('language__btn', (ctx, json) => {
        ctx.tag('a').attrs({
            'href': '#',
            'id': 'language',
            'data-toggle': 'dropdown',
            'aria-haspopup': 'true',
            'aria-expanded': 'false',
        });
    });
};
