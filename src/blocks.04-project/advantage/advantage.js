const ClassName = {
    BLOCK: 'advantage',
    SLIDE: 'advantage__slide',
    IMG: 'advantage__img',
};
const addImg = function(instance) {
    Array.from(instance.slides[instance.activeIndex].getElementsByClassName(ClassName.IMG)).forEach((img)=>{
        let hiddenIMG = new Image();
        hiddenIMG.setAttribute('src', img.dataset['src']);
        hiddenIMG.addEventListener('load', (event) => {
            img.style.backgroundImage = `url(${event.target.src})`;
            img.style.opacity = '';
            hiddenIMG.remove();
        });
        document.body.appendChild(hiddenIMG);
    });
};


(($)=>{
    Array.from(document.getElementsByClassName(ClassName.BLOCK)).map((block, index)=>{
        let instance = $(block).data('swiper');
        // let firstImg = block.getElementsByClassName(ClassName.SLIDE)[0].getElementsByClassName(ClassName.IMG)[0];
        // firstImg.style.backgroundImage = firstImg.dataset['src'];

        instance.on('transitionStart', () => {
            addImg(instance);
        });
        instance.on('slideChange', () => {
            addImg(instance);
        });
    });
})($);
