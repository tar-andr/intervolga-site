module.exports = (bh) => {
    bh.match('tab', (ctx, json) => {
        ctx.tParam('ID', ctx.generateId());
        ctx.attr('id', ctx.attr('id') ? ctx.attr('id') + ctx.tParam('ID') : ctx.tParam('ID'), true);
    });
};
