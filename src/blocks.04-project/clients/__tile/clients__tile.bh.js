module.exports = (bh) => {
    bh.match('clients__tile', (ctx, json) => {
        ctx.tag('a').attrs({
            href: '#',
            target: '_blank',
        }, true);
    });
};
