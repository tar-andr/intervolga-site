module.exports = (bh) => {
    bh.match('about-project__backside', (ctx, json) => {
        ctx.tag('a').attrs({
            href: '#',
            target: '_blank',
        });
    });
};
