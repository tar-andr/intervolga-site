module.exports = (bh) => {
    bh.match('search__btn', (ctx, json) => {
        ctx.tag('button').attrs({
            type: 'search',
        });
    });
};
