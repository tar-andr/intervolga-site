module.exports = (bh) => {
    bh.match('search__input', (ctx, json) => {
        ctx.tag('input').attrs({
            type: 'text',
            name: 'search',
            placeholder: 'Поиск по сайту',
        });
    });
};
