module.exports = (bh) => {
    bh.match('preview__link', (ctx, json) => {
        ctx.tag('a').attrs({
            href: '#',
        }, true);
    });
};
