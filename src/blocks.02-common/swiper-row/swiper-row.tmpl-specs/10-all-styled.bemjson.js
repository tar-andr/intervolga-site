module.exports = [
    {block: 'swiper-row', cls: 'row', content: [
        {cls: 'w-100', content: [
            {cls: 'swiper-container swiper-container-horizontal', content: [
                {cls: 'swiper-wrapper', content: ((item) => new Array(12).fill(item))([
                    {block: 'col-6', cls: 'col-sm-6 col-md-4 col-lg-6 col-xl-4 swiper-slide h-auto', content: [
                        'Слайд',
                    ]},
                ])},
                {cls: 'swiper-pagination'},
            ]},
        ]},
    ]},
];
