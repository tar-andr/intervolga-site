import Swiper from 'swiper/dist/js/swiper.min';
(($) => {
    let instance = Array.from(document.getElementsByClassName('swiper-row')).map((block)=>{
        return $(block).data('swiper', new Swiper(block.getElementsByClassName('swiper-container')[0], {
            slidesPerView: 'auto',
            spaceBetween: 0,
            watchSlidesVisibility: true,
            slidesPerGroup: 1,
            autoplay: block.getElementsByClassName('swiper-container')[0].dataset['delay'] && {
                delay: block.getElementsByClassName('swiper-container')[0].dataset['delay'],
            },
            navigation: {
                nextEl: block.getElementsByClassName('swiper-row__button_next'),
                prevEl: block.getElementsByClassName('swiper-row__button_prev'),
            },
            pagination: {
                el: block.getElementsByClassName('swiper-pagination'),
                type: 'bullets',
                clickable: true,
            },
        })).data('swiper');
    });
    $(document).on('ready shown.bs.tab shown.bs.collapse shown.bs.modal', (event) => {
        instance.forEach((swiper) => {
            swiper.update();
        });
    });
})($);
