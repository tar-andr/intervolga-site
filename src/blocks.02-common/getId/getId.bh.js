module.exports = (bh) => {
    bh.match('getId', (ctx, json) => {
        ctx.tParam('ID', ctx.generateId());
        return ctx.content();
    });
};
