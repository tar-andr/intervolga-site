module.exports = {
    mustDeps: [
        {block: 'utilities'},
    ],
    shouldDeps: [
        {elem: 'placemark'},
    ],
};
